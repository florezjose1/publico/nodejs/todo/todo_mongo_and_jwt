var mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Setup schema
var todoSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true,
        default: 'new'
    },
    color: {
        type: String,
        required: true,
        default: 'red'
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    limit_date:{
        type: Date,
        default: Date.now
    },
    private: {
        type: Boolean,
        default: true,
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'userId',
        required: true
    }
});

// Export Contact model
var Todo = module.exports = mongoose.model('todo', todoSchema);

module.exports.get = function (callback, limit) {
    Todo.find(callback).limit(limit);
};
