// Import todo model
Todo = require('./todoModel');
// Handle index actions
exports.index = async (req, res) => {
    const _id = req.body.userId;

    try {
        const noteDB = await Todo.find({userId:_id});
        res.json({
            status: "success",
            message: "Todo retrieved successfully",
            data:noteDB
        });

    } catch (error) {
        return res.status(404).json({
            message: 'Task empty!',
            error
        })
    }
};

// Handle create todo actions
exports.new = async (req, res) => {
    const body = req.body;
    try {
        const noteDB = await Todo.create(body);
        res.status(200).json(noteDB);
    } catch (error) {
        return res.status(500).json({
            message: 'Something wen wrong',
            error
        })
    }
};
// Handle view todo info
exports.view = async (req, res) => {
    const _id = req.params.todo_id;
    try {
        const noteDB = await Todo.findOne({_id});
        res.json(noteDB);
    } catch (error) {
        return res.status(404).json({
            message: 'Note no exist!',
            error
        })
    }
};
// Handle update todo info
exports.update = function (req, res) {
    Todo.findById(req.params.todo_id, function (err, todo) {
        if (err)
            res.send(err);
        todo.title = req.body.title ? req.body.title : todo.title;
        todo.description = req.body.description;
        todo.color = req.body.color;
        todo.state = req.body.state;
        // save the todo and check for errors
        todo.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Todo Info updated',
                data: todo
            });
        });
    });
};
// Handle delete todo
exports.delete = function (req, res) {
    Todo.remove({
        _id: req.params.todo_id
    }, function (err, todo) {
        if (err)
            res.send(err);
        res.json({
            status: 204,
            message: 'Todo deleted'
        });
    });
};
