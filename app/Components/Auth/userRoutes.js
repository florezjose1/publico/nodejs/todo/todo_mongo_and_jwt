const express = require('express');
const router = express.Router();
const userController = require('./userController');

router.post('/user/register', userController.create);
router.post('/user/authenticate', userController.authenticate);

module.exports = router;