// Import express
let express = require('express');
let cors = require('cors');
const logger = require('morgan');
var jwt = require('jsonwebtoken');

const mongoose = require('./config/database'); //database configuration

// Import Body parser
let bodyParser = require('body-parser');
// Initialise the app
let app = express();

app.set('secretKey', 'nodeRestApi'); // jwt secret token

// Import routes
let apiRoutes = require("./api-routes");
let todoRoutes = require("./app/Components/Todo/todoRoutes");
let userRoutes = require("./app/Components/Auth/userRoutes");

// cors
app.use(cors());

app.use(logger('dev'));

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var db = mongoose.connection;

// Added check for DB connection
if(!db)
    console.log("Error connecting db");
else
    console.log("Db connected successfully");

// Setup server port
var port = process.env.PORT || 8080;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Use Api routes in the App
app.use('/api', apiRoutes);
app.use('/api', userRoutes);
app.use('/api', validateUser, todoRoutes);

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
        if (err) {
            res.json(status=401, {status:"error", message: err.message, data:null});
        }else{
            // add user id to request
            req.body.userId = decoded.id;
            next();
        }
    });

}

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});
